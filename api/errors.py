class APIError(Exception):
    """General API Error"""

    status_code = 500

    def get_payload(self):
        return {}


class FeatureDisabled(APIError):
    """Returns HTTP code 503"""

    status_code = 503


class BadRequest(APIError):
    """Returns HTTP code 400"""

    status_code = 400


class FailedLogin(APIError):
    """Returns HTTP code 401, rate-limits the IP if the rate-limit is exceeded"""

    status_code = 401
    # TODO: [APITHESQL-3] rate-limit failed logins as brute-force protection


class FailedUpload(APIError):
    """Returns HTTP code (please look into what would be suitable)"""

    status_code = 500  # 500 for now

