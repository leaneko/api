from api.errors import BadRequest
from cerberus import Validator


def Validate(document, schema):
    if document is None:
        return False

    schema = Validator(schema)
    valid = schema.validate(document)

    if not valid:
        raise BadRequest(schema.errors)
    else:
        return valid


_META_TYPE_USER_ID = {
    "type": "string",
    "required": True,
    "regex": "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}",
}
_META_TYPE_USER_ID_OPTIONAL = {
    "type": "string",
    "required": False,
    "regex": "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}",
}


_META_TYPE_USERNAME = {
    "type": "string",
    "required": True,
    "minlength": 3,
    "maxlength": 64,
}
_META_TYPE_USERNAME_OPTIONAL = {
    "type": "string",
    "required": False,
    "minlength": 3,
    "maxlength": 64,
}

_META_TYPE_EMAIL = {
    "type": "string",
    "required": True,
    "regex": "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
}
_META_TYPE_EMAIL_OPTIONAL = {
    "type": "string",
    "required": False,
    "regex": "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
}

_META_TYPE_PASSWORD = {
    "type": "string",
    "required": True,
    "minlength": 8,
    "maxlength": 256,
}
_META_TYPE_PASSWORD_OPTIONAL = {
    "type": "string",
    "required": True,
    "minlength": 8,
    "maxlength": 256,
}


REGISTRATION_SCHEMA = {
    "username": _META_TYPE_USERNAME,
    "password": _META_TYPE_PASSWORD,
    "email": _META_TYPE_EMAIL,
}

LOGIN_SCHEMA = {
    "username": _META_TYPE_USERNAME,
    "password": _META_TYPE_PASSWORD,
}

ADMIN_ACTIVATE_USER_SCHEMA = {"user_id": _META_TYPE_USER_ID}

UPDATE_USER_SCHEMA = {
    "email": _META_TYPE_EMAIL_OPTIONAL,
    "password": _META_TYPE_PASSWORD_OPTIONAL,
    "new_password": _META_TYPE_PASSWORD_OPTIONAL,
}

CREATE_UPLOAD_TOKEN_SCHEMA = {"password": _META_TYPE_PASSWORD}
DELETE_UPLOAD_TOKEN_SCHEMA = {"password": _META_TYPE_PASSWORD}

RESET_PASSWORD_SCHEMA_GENERIC = {
    "action": {"type": "string", "required": True, "allowed": ["request", "reset"]},
    "email": {"type": "string", "required": False},
    "password": {"type": "string", "required": False},
}
RESET_PASSWORD_SCHEMA_REQUEST = {
    "action": {"type": "string", "required": True, "allowed": ["request"]},
    "email": _META_TYPE_EMAIL,
}
RESET_PASSWORD_SCHEMA_ACTION = {
    "action": {"type": "string", "required": True, "allowed": ["reset"]},
    "password": _META_TYPE_PASSWORD,
}
