from quart import Blueprint, Response
from quart import json
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from ..schema import Validate, LOGIN_SCHEMA, REGISTRATION_SCHEMA
from ..errors import BadRequest, FailedLogin, FeatureDisabled
from ..common import identifiers
from ..common.auth import (
    check_credentials,
    check_token,
    create_token,
    user_exists,
    create_user,
)

from .. import storage

import bcrypt

bp = Blueprint("auth", __name__)


@bp.route("/register", methods=["POST"])
@bp.route("/signup", methods=["POST"])
async def register():
    """This is the route called when /register or /signup is hit with a POST request.

    Raises:
        FeatureDisabled: This returns a HTTP 503 error, raised when registrations are disabled.
        BadRequest: Returns a HTTP 400 error, raised either when the request fails to validate, or when the user already exists.

    Returns:
        Response: A JSONify response object [see: create_user()]
    """
    if not app.api_config.REGISTRATIONS_ENABLED:
        raise FeatureDisabled("Registrations are currently disabled")

    req = await request.json

    if not Validate(req, REGISTRATION_SCHEMA):
        raise BadRequest("Bad request contents")

    if await user_exists(req.get("email"), req.get("username")):
        raise BadRequest("A user with this email or username already exists")

    return await create_user(req)


@bp.route("/login", methods=["POST"])
async def login():
    """This is the route called when /login is with with a POST request.

    Raises:
        BadRequest: Returns a HTTP 400 error, raised when the request fails to validate.
        FailedLogin: Returns a HTTP 401 error, raised either when the user does not exist, or an invalid username-password combination is given

    Returns:
        Response: A JSONify response object [see: create_token()]
    """
    req = await request.json

    if not Validate(req, LOGIN_SCHEMA):
        raise BadRequest("Bad request contents")

    if not await user_exists(None, req.get("username")):
        raise FailedLogin("Invalid credentials")

    if not await check_credentials(req.get("username"), req.get("password")):
        raise FailedLogin("Invalid credentials")

    user_id = await storage.get_user_id_by_username(req.get("username"))

    return jsonify({"token": await create_token(user_id)})
