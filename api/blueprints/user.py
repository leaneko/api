from quart import Blueprint, Response
from quart import json
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from .. import storage
from ..schema import (
    Validate,
    RESET_PASSWORD_SCHEMA_ACTION,
    RESET_PASSWORD_SCHEMA_GENERIC,
    RESET_PASSWORD_SCHEMA_REQUEST,
    UPDATE_USER_SCHEMA,
    CREATE_UPLOAD_TOKEN_SCHEMA,
    DELETE_UPLOAD_TOKEN_SCHEMA,
)
from ..errors import BadRequest, FailedLogin
from ..common.auth import (
    check_credentials,
    check_token,
    create_token,
    delete_unverified_user,
    hash_password,
    update_password,
    verify_user,
)
from ..common.email import send_mail

bp = Blueprint("user", __name__)


@bp.route("/user", methods=["GET"])
async def get_user_route():
    """Fetches information about the currently-authenticated user."""
    user_id = await check_token(request)
    return jsonify(await storage.get_user(identifier=user_id))


@bp.route("/user/verify", methods=["GET"])
async def verify_user_route():
    """When paired with a token query parameter, verifies the email of an account."""
    user_id = await check_token(request)
    await verify_user(user_id)

    return jsonify({"success": True})


@bp.route("/user/verify/delete", methods=["GET", "DELETE"])
async def delete_unverified_user_route():
    user_id = await check_token(request)
    await delete_unverified_user(user_id)

    return jsonify({"success": True})


@bp.route("/reset_password", methods=["POST"])
async def _reset_user_password_route():
    req = await request.json
    if not Validate(req, RESET_PASSWORD_SCHEMA_GENERIC):
        raise BadRequest("Invalid data.")

    action = req["action"].lower()
    if action == "request":
        if not Validate(req, RESET_PASSWORD_SCHEMA_REQUEST):
            raise BadRequest("Invalid data.")

        user = await storage.get_user(id_type="email", identifier=req["email"])
        await send_mail(user, "password_reset")

        return jsonify({"success": True})
    elif action == "reset":
        if not Validate(req, RESET_PASSWORD_SCHEMA_ACTION):
            raise BadRequest("Invalid data.")

        user = await storage.get_user(identifier=await check_token(request))
        await update_password(user, req["password"])

        return jsonify({"success": True})


@bp.route("/user", methods=["PATCH", "POST"])
async def modify_user_route():
    """Allows a user to modify their data."""
    req = await request.json
    if not Validate(req, UPDATE_USER_SCHEMA):
        raise BadRequest("Invalid data.")

    if (
        "new_password" in req
        and "password" not in req
        or "email" in req
        and "password" not in req
    ):
        raise BadRequest(
            "To update your email or password, you must first provide your current password."
        )

    user = await storage.get_user(identifier=await check_token(request))

    if not await check_credentials(user["username"], req["password"]):
        raise FailedLogin("Invalid credentials.")
    # TODO: Allow user to update their information (email, password, preferences)
    return jsonify({"current_info": user})


@bp.route("/user/tokens", methods=["PUT", "POST"])
async def create_user_tokens_route():
    """Creates a non-timed ("upload") authentication token."""
    req = await request.json

    if not Validate(req, CREATE_UPLOAD_TOKEN_SCHEMA):
        raise BadRequest("Invalid data.")

    user = await storage.get_user(identifier=await check_token(request))

    if not await check_credentials(user["username"], req["password"]):
        raise FailedLogin("Invalid credentials.")

    return jsonify({"token": await create_token(user["id"], type="nontimed")})


@bp.route("/user/tokens", methods=["DELETE"])
async def delete_user_tokens_route():
    """Revokes all timed & non-timed authentication tokens."""
    req = await request.json

    if not Validate(req, DELETE_UPLOAD_TOKEN_SCHEMA):
        raise BadRequest("Invalid data.")

    user = await storage.get_user(identifier=await check_token(request))

    if not await check_credentials(user["username"], req["password"]):
        raise FailedLogin("Invalid credentials.")

    await update_password(user, req.get("password"))

    return jsonify({"success": True})


@bp.route("/user/uploads", methods=["GET"])
async def get_user_uploads_route():
    """Gets all of the uploads for the currently-authenticated user"""
    user_id = await check_token(request)
    return jsonify(await storage.get_user_uploads(user_id))
