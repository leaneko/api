import config

from quart import Quart
from quart.json import jsonify

from minio import Minio

import asyncpg as db

from api.blueprints import auth, user, upload, admin
from api.errors import APIError
from api import schema

app = Quart(__name__)
app.api_config = config

app.config["JSON_SORT_KEYS"] = False
app.config["MAX_CONTENT_LENGTH"] = config.MAXIMUM_SYSTEMWIDE_UPLOAD_LIMIT * 1024 * 1024


@app.before_serving
async def app_before_serving():
    app.db = await db.create_pool(**config.DATABASE_INFORMATION)
    app.minio = Minio(**config.S3_INFORMATION)


@app.errorhandler(APIError)
def handle_api_error(exception):
    """Handle any kind of application-level raised error."""

    scode = exception.status_code
    res = {"error": True, "code": scode, "message": exception.args[0]}

    res.update(exception.get_payload())

    res = jsonify(res)
    res.status_code = scode
    return res


@app.errorhandler(Exception)
def handle_exception(exception):
    """Handle any kind of exception."""
    status_code = 500

    try:
        status_code = exception.status_code
    except AttributeError:
        pass

    res = jsonify({"error": True, "message": repr(exception)})
    res.status_code = status_code
    return res


def set_blueprints(app_):
    app_.register_blueprint(auth.bp)
    app_.register_blueprint(user.bp)
    app_.register_blueprint(upload.bp)
    app_.register_blueprint(admin.bp)


set_blueprints(app)


def main():
    """Main Application Entrypoint"""
    app.run(host=config.HOST, port=config.PORT)


if __name__ == "__main__":
    main()
